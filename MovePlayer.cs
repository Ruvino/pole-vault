using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// �����, ������� ��������� ������������ ������
public class MovePlayer : MonoBehaviour
{
    public Rigidbody rb;
    public float runSpeed = 7f;
    public Animator animatorPlayer;
    public float movementSideSpeed = 10f;
    float movement = 0f;
    public Transform pole;

    float timeToStay;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        rb.MovePosition(transform.position + transform.forward * runSpeed * Time.fixedDeltaTime);

        movement = Input.GetAxis("Horizontal") * movementSideSpeed;
        Vector3 velocity = rb.velocity;
        velocity.x = movement;
        rb.velocity = velocity;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("SuccessJump"))
        {
            runSpeed = 10f;
            Destroy(other.gameObject);
            print("�� ������� ������������!");
            animatorPlayer.SetBool("isJump", false);
        }
        if (other.CompareTag("AreaBeforeJump"))
        {
            runSpeed = 8f;
            Destroy(other.gameObject);
            if (pole.localScale.y > 3f)
            {
                pole.localScale = new Vector3(pole.localScale.x, 3f, pole.localScale.z);
            }
            animatorPlayer.SetBool("poleBefore", true);
        }
    }
}
