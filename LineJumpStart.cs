using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// �����, ������� ��������� �������� ��� ������� � ���� ��������� 
public class LineJumpStart : MonoBehaviour
{
    public Transform transformTarget;
    public GameObject player;
    public Transform playerTransform;
    public Rigidbody playerRigidbody;
    public Animator anim;
    public Transform lenghPole;
    private Vector3 forceUP;
    public LayerMask LayerMask;
    private Transform m_Transform;

    private void Awake()
    {
        m_Transform = GetComponent<Transform>();
        playerTransform = player.GetComponent<Transform>();
        playerRigidbody = player.GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        // ��� ����������� ����� ������ ������ ������� ������ �����.
        if (Physics.Linecast(m_Transform.position, transformTarget.position, LayerMask))
        {
            Destroy(this.gameObject);
            Destroy(transformTarget.gameObject);
            anim.SetBool("isJump", true);
            anim.SetBool("poleBefore", false);
            print("����� ������ ����� � ����� �������!");
            if (lenghPole.localScale.y >= 2f)
            {
                forceUP = new Vector3(0f, 14f, 14f);
            }
            else
            {
                forceUP = new Vector3(0f,5f,5f);
            }
            playerRigidbody.AddForce(forceUP, ForceMode.VelocityChange);
            lenghPole.gameObject.SetActive(false);
        }
    }
}
