using UnityEngine;
using System.Collections;

// ����� ������� ��������� �������� ������ �� �������
public class FollowCamera : MonoBehaviour {
	
	public GameObject target;
	public float damping = 1;
	Vector3 offset;
	
	void Start() {

		offset = target.transform.position - transform.position;
	}
	
	void LateUpdate() {

		float currentAngle = transform.eulerAngles.y;
		float desiredAngle = target.transform.eulerAngles.y;
		float angle = Mathf.LerpAngle(currentAngle, desiredAngle, Time.deltaTime * damping);
		
		transform.position = target.transform.position - offset;
		
		transform.LookAt(target.transform);
	}
}